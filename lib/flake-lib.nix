inputs@{ pkgs, home, ... }:
rec {
  pkgsFor = system: import pkgs {
    inherit system;
    config = {
      allowUnfree = true;
    };
    overlays = with inputs; [
      emacs.overlay
      (super: self: import ../pkgs { pkgs = super; })
    ];
  };

  mkUserModules =
    { username
    , hostname
    , groups
    , extraAttrs ? ({ ... }: {})
    , extraHomeModules ? ({ ... }: [])
    , extraSystemModules ? ({ ... }: [])
    }: let
      userModule = ../. + "/modules/home/default.nix";
      hostModule = ../. + "/hosts/${hostname}/home.nix";
    in ([
      ({ pkgs, ... }: {
        users.extraUsers.${username} = {
          isNormalUser = true;
          extraGroups = groups;
        } // (extraAttrs { inherit username hostname pkgs; });
        home-manager = {
          useGlobalPkgs = true;
          useUserPackages = true;
          users.${username} = {
            imports = [ userModule hostModule ] ++
                      (extraHomeModules { inherit username hostname pkgs inputs; });
            home = {
              inherit username;
              homeDirectory = "/home/${username}";
              extraOutputsToInstall = [ "doc" "man" ];
              stateVersion = "21.03";
            };
            programs.home-manager.enable = true;
          };
        };
      })
    ] ++ (extraSystemModules { inherit username hostname inputs pkgs; }));

  mkSystems = builtins.mapAttrs (k: v: mkSystem k v);
  mkSystem = hostname: { system ? "x86_64-linux", extraModules ? ({ ... }: [ ]) }:
    let
      pkgs = pkgsFor system;
      hostModulePath = ../. + "/hosts/${hostname}/default.nix";
      globalConfig = {
        nix = {
          registry.nixpkgs.flake = inputs.pkgs;
          nixPath = [
            "nixpkgs=${inputs.pkgs}"
            "home-manager=${inputs.home}"
            "/nix/var/nix/profiles/per-user/root/channels"
          ];
          binaryCaches = [
            "https://nix-community.cachix.org"
          ];
          binaryCachePublicKeys = [
            "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
          ];
        };
        networking = {
          hostName = hostname;
          hostId = pkgs.lib.strings.substring 0 8
            (builtins.hashString "sha1" hostname);
        };
        nixpkgs.pkgs = pkgs;
        system.stateVersion = "21.03";
        users.mutableUsers = false;
      };
    in
    inputs.pkgs.lib.nixosSystem {
      inherit system;
      modules = [
        globalConfig
        hostModulePath
        inputs.home.nixosModules.home-manager
      ] ++ (extraModules {
        inherit system pkgs hostname inputs;
      });
    };
}
