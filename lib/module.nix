let
  endsWith = ext: str: (builtins.match ".*${ext}" str != null);
  modDir = ../modules;
  _getModule = name: if endsWith "/" name then modDir + "/${name}/default.nix"
                    else modDir + "/${name}.nix";
in rec {
  getModule = name: _getModule "nixos/${name}";
  getHomeModule = name: _getModule "home/${name}";
}
