{
  description = "system configuration";
  inputs = {
    pkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    hardware.url = "github:NixOS/nixos-hardware";
    emacs.url = "github:nix-community/emacs-overlay";
    impermanence.url = "github:nix-community/impermanence";
    home = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "pkgs";
    };
  };
  outputs = inputs:
    let
      lib = (import ./lib).flake inputs;
      userDef = hostname: {
        username = "lyla";
        inherit hostname;
        groups = [ "wheel" "networkmanager" "audio" "video" "input" ];
        extraAttrs = { ... }: {
          description = "Lyla Bravo";
          initialHashedPassword = "$6$STSm4pLJ5KoLy$NNUVd8mRfrAVSlaNo10bYVnBMclKXWCnyXrhbuG3zqDiNeSz0Rp92GFTV9xkZXBdvsrl6fXNYKLd1nSyLGjLv/";
        };
        extraHomeModules = { ... }: [];
        extraSystemModules = { username, pkgs, ... }: [
          {
            environment.persistence."/nix/persist".directories = pkgs.lib.forEach
              [ "src" "usr" ".ssh" ".emacs.d" ".cache" ".mozilla" ]
              (dir: "/home/${username}/${dir}");
          }
        ];
      };
    in with lib; {
      nixosConfigurations = mkSystems {
        crow = {
          system = "x86_64-linux";
          extraModules = { inputs, hostname, ... }: [
            inputs.hardware.nixosModules.lenovo-thinkpad-t420
            inputs.hardware.nixosModules.common-pc-laptop-ssd
            inputs.impermanence.nixosModules.impermanence
          ] ++ mkUserModules (userDef hostname);
        };
        sergal = {
          system = "x86_64-linux";
          extraModules = { inputs, hostname, ... }: [
            inputs.hardware.nixosModules.common-pc-hdd
            inputs.impermanence.nixosModules.impermanence
          ] ++ mkUserModules (userDef hostname);
        };
              
      };
    };
}
