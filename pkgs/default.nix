{ pkgs ? import <nixpkgs> {} }:
{
  adwaita-slim = pkgs.callPackage ./adwaita-slim.nix {};
}
