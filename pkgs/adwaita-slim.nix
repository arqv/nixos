{ stdenvNoCC, fetchFromGitHub }:
stdenvNoCC.mkDerivation {
  name = "adwaita-slim";
  src = fetchFromGitHub {
    owner = "ArchByte";
    repo = "Adwaita-Slim";
    rev = "40ad54f5f81fce1fd6ef131568ca14caa8dbea5e";
    sha256 = "sha256-lOD3G0GxvDQkduBLiRNDb4FE00GVoc1tiCWDO3rlbsk=";
  };
  src2 = fetchFromGitHub {
    owner = "ArchByte";
    repo = "Adwaita-Slim";
    rev = "2b246dfbcf8b26783b7a8f974e5fb0a8fb2e4d0a";
    sha256 = "sha256-GpGr6HHliWAYt/1hII1S3jvpGzxA9fzHtZhjxQB4Fyg=";
  };
  dontConfigure = true;
  dontBuild = true;
  installPhase = ''
    mkdir -p $out/share/themes/Adwaita-Slim-Light/gtk-3.0/
    mkdir -p $out/share/themes/Adwaita-Slim-Dark/gtk-3.0/
    cp -r $src/* $out/share/themes/Adwaita-Slim-Light/gtk-3.0/
    cp -r $src2/* $out/share/themes/Adwaita-Slim-Dark/gtk-3.0/
   '';
}
