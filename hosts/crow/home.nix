{ inputs, lib, ... }:
let
  module = (import ../../lib/module.nix).getHomeModule;
  modules = ml: lib.forEach ml (m: module m);
in
{
  imports = modules [
    "base/desktop"
  ];
}
