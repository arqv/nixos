{ inputs, ... }:
let
  module = (import ../../lib/module.nix).getModule;
in
{
  imports = [
    ./hardware.nix
    (module "base/")
    (module "base/desktop")
  ];
  time.timeZone = "America/Santiago";
  i18n.defaultLocale = "en_US.UTF-8";
  services.xserver.layout = "latam";
}
