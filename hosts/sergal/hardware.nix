{ config, pkgs, lib, ... }:
{
  hardware.enableRedistributableFirmware = true;

  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sda";
      memtest86.enable = true;
      useOSProber = true;
    };
    kernelPackages = pkgs.linuxPackages_latest;
    kernelParams = [];
    kernelModules = [ "kvm-intel" "acpi_call" ];
    extraModulePackages = with config.boot.kernelPackages; [
      acpi_call
    ];
    initrd = {
      availableKernelModules = [
        "ehci_pci"
        "ahci"
        "sd_mod"
        "sr_mod"
        "rtsx_pci_sdmmc"
      ];
      kernelModules = [ ];
      postDeviceCommands = lib.mkAfter ''
        zfs rollback -r tank/local/ephemeral/root@blank
        # zfs rollback -r tank/local/ephemeral/home@blank
      '';
    };
  };

  fileSystems = {
    "/boot" = {
      device = "/dev/sdb2";
      fsType = "ext4";
    };
    "/" = {
      device = "tank/local/ephemeral/root";
      fsType = "zfs";
    };
    "/nix" = {
      device = "tank/local/nix";
      fsType = "zfs";
    };
    "/var" = {
      device = "tank/local/var";
      fsType = "zfs";
    };
    "/nix/persist" = {
      device = "tank/persist";
      fsType = "zfs";
      neededForBoot = true;
    };
    "/home" = {
      device = "tank/local/ephemeral/home";
      fsType = "zfs";
    };
    "/nix/persist/home" = {
      device = "tank/persist/home";
      fsType = "zfs";
      neededForBoot = true;
    };
  };
  swapDevices = [];
  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
}
