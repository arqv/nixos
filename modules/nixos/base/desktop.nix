{ pkgs, lib, ... }:
{
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };

  console.useXkbConfig = true;

  programs = {
    slock.enable = true;
    light.enable = true;
  };
  
  services = {
    xserver = {
      enable = true;
      libinput.enable = true;
      displayManager = {
        /* defaultSession = "xsession";
        session = [{
          manage = "desktop";
          name = "xsession";
          start = "exec $HOME/.xsession-hm";
        }]; */
        lightdm = {
          enable = true;
          background = ./wallpaper.jpg;
        };
      };
      # windowManager.berry.enable = true;
      windowManager.openbox.enable = true;
    };    
    picom = {
      enable = true;
    };
  };

  fonts = {
    fonts = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      inter source-code-pro
      recursive
      corefonts
    ];
    fontconfig = {
      enable = true;
      subpixel.rgba = "none";
      defaultFonts = with lib; {
        sansSerif = mkForce [ "Inter" "Noto Sans" ];
        serif = mkForce [ "Noto Serif" ];
        monospace = mkForce [ "Source Code Pro" ];
      };
    };
  };

  environment.systemPackages = with pkgs; [
    firefox
    libreoffice
    xclip maim slop xorg.xmodmap
  ];
}
