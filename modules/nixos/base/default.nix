{ pkgs, lib, ... }:
{
  users.defaultUserShell = pkgs.zsh;
  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  networking.networkmanager.enable = true;

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = ca-references nix-command flakes
    '';
    trustedUsers = [ "root" "@wheel" ];
    autoOptimiseStore = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
    distributedBuilds = true;
    buildMachines = [
      {
        hostName = "eu.nixbuild.net";
        system = "x86_64-linux";
        maxJobs = 100;
        supportedFeatures = [ "benchmark" "big-parallel" ];
      }
    ];
  };
  
  programs = {
    ssh = {
      knownHosts = {
        nixbuild = {
          hostNames = [ "eu.nixbuild.net" ];
          publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPIQCZc54poJ8vqawd8TraNryQeJnvH1eLpIDgbiqymM";
        };
      };
      extraConfig = ''
        Host eu.nixbuild.net
          PubkeyAcceptedKeyTypes ssh-ed25519
          IdentityFile /root/.ssh/nixbuild.key
      '';
    };
    dconf.enable = true;
    zsh = {
      enable = true;
      autosuggestions.enable = true;
      syntaxHighlighting.enable = true;
      promptInit = lib.mkForce ''
        source ${pkgs.liquidprompt}/bin/liquidprompt
      '';
      interactiveShellInit = ''
        source ${pkgs.zsh-nix-shell}/share/zsh-nix-shell/nix-shell.plugin.zsh
        eval "$(${pkgs.direnv}/bin/direnv hook zsh)"
      '';
    };
  };

  services = {
    lorri.enable = true;
    tlp.enable = true;
  };

  environment = {
    homeBinInPath = true;
    persistence."/nix/persist".directories = [
      "/root"
    ];
    systemPackages = with pkgs; [
      exa fd ripgrep
      git gnupg
      pandoc direnv
    ];
    shellAliases = rec {
      ls = "exa -Fs Name --group-directories-first";
      la = "exa -Fals name --git --group-directories-first";
      ll = "exa -Fls Name --git --group-directories-first";
      l = la;
    };

    etc = {
      "NetworkManager/system-connections".source =
        "/nix/persist/etc/NetworkManager/system-connections";
      "liquidprompt".text = ''
        LP_ENABLE_BATT=0
        LP_ENABLE_LOAD=0
        LP_ENABLE_TITLE=1
      '';
    };
  };
}
