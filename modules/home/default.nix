{ pkgs, config, ... }:

let
  module = (import ../../lib/module.nix).getHomeModule;
in {
  imports = [
    (module "base/")
    (module "asdf")
  ];

  xdg = {
    enable = true;
    # configHome = "${config.home.homeDirectory}/etc";
    # cacheHome = "${config.home.homeDirectory}/.cache";
    # dataHome = "${config.home.homeDirectory}/etc/local";
    userDirs = {
      enable = true;
      desktop = "$HOME/usr/desk";
      documents = "$HOME/usr/doc";
      download = "$HOME/usr/dl";
      music = "$HOME/usr/mus";
      pictures = "$HOME/usr/vis";
      publicShare = "$HOME/.trash";
      templates = "$HOME/.trash";
      videos = "$HOME/usr/vis";
    };
  };
}
