{ config, pkgs, ... }:
{
  home = {
    packages = [
      pkgs.ag
    ];
    sessionVariables = {
      EDITOR = "emacsclient";
      IRLITE_DIR = "$HOME/src/irlite.d";
    };
  };
  
  programs.emacs = {
    enable = true;
    package = pkgs.emacsGit; /* .override {
      withGTK2 = false;
      withGTK3 = true;
      withXwidgets = true;
    }; */
    extraPackages = epkgs: with epkgs; [
      evil
      exwm desktop-environment ednc gcmh
      use-package diminish no-littering
      # ivy swiper counsel ivy-posframe
      selectrum orderless prescient selectrum-prescient consult
      hydra avy
      projectile direnv
      which-key which-key-posframe
      lsp-mode
      company company-posframe
      treemacs treemacs-projectile
      flycheck
      elnode
      magit
      treemacs-magit
      edwina
      posframe
      vterm
      modus-themes doom-themes
      nix-mode zig-mode web-mode impatient-mode lispy auctex sly
      circe circe-notifications
      auctex
    ];
    overrides = self: super: {
      auctex = super.elpaPackages.auctex;
      exwm = super.exwm;
    };
  };
}
