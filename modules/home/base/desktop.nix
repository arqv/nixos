{ config, pkgs, ... }: {
  imports = [
    ./emacs.nix ./polybar.nix
  ];

  home.file = {
    ".Xmodmap".text = ''
      ! remap Menu key to Hyper
      clear mod3
      keycode 135 = Hyper_R
      add mod3 = Hyper_R
    '';
  };
  
  xsession = {
    enable = true;
    scriptPath = ".xsession";
    pointerCursor = {
      package = pkgs.vanilla-dmz;
      name = "Vanilla-DMZ";
      size = 16;
    };
    initExtra = ''
      # check if Emacs server is running
      if emacsclient -a false -e t 2>/dev/null >/dev/null; then
        echo "yes" | emacsclient -a false -e '(server-mode -1)'
      fi
      emacs --daemon
      xmodmap $HOME/.Xmodmap
    '';
    windowManager.command = let
      emacs = config.programs.emacs.finalPackage;
    in "${pkgs.dbus}/bin/dbus-launch --exit-with-session ${emacs}/bin/emacsclient -c";
  };

  gtk = {
    enable = true;
    iconTheme = {
      package = pkgs.elementary-xfce-icon-theme;
      name = "elementary-xfce-dark";
    };
    theme = {
      package = pkgs.adwaita-slim;
      name = "Adwaita-Slim-Dark";
    };
  };
}
