{ config, pkgs, ... }: {
  services.polybar = {
    enable = true;
    script = "polybar top &";
    package = pkgs.polybar.override {
      pulseSupport = true;
    };
    config = {
      "bar/top" = {
        background = "\${env:POLYBAR_BG:#ffffff}";
        foreground = "\${env:POLYBAR_FG:#000000}";
        font-0 = "Iosevka Curly Extended:Recursive Mono Linear Static:pixelsize=9;2";
        font-1 = "Iosevka Curly Extended:pixelsize=9:weight=bold;2";
        font-2 = "Iosevka Aile:pixelsize=9;2";
        font-3 = "Iosevka Aile:pixelsize=9:weight=bold;2";
        enable-ipc = true;
        width = "100%";
        height = 16;
        padding = 1;
        module-margin-left = 1;
        modules-left = "exwm";
        modules-right = "volume cpu ram battery date";
      };
      "module/volume" = {
        type = "internal/pulseaudio";
        format-volume = "\${env:POLYBAR_VOLUME}";
        format-muted = "\${env:POLYBAR_VOLUME_MUTED}";
        label-volume = "%percentage%%";
        label-muted = "muted";
      };
      "module/cpu" = {
        type = "internal/cpu";
        format = "\${env:POLYBAR_CPU}";
        label = "%percentage%%";
      };
      "module/ram" = {
        type = "internal/memory";
        format = "\${env:POLYBAR_MEMORY}";
        label = "%percentage_used%%/%percentage_swap_used%%";
      };
      "module/battery" = {
        type = "internal/battery";
        battery = "BAT1";
        adapter = "ACAD";
        format-charging = "\${env:POLYBAR_BATTERY_CHARGING}";
        format-discharging = "\${env:POLYBAR_BATTERY_DISCHARGING}";
        format-full = "\${env:POLYBAR_BATTERY_FULL}";
        label-charging = "%percentage%%";
        label-discharging = "%percentage%%";
        label-full = "%percentage%%";
      };
      "module/exwm" = pkgs.lib.mkIf config.programs.emacs.enable {
        type = "custom/ipc";
        hook-0 = ''
          ${config.programs.emacs.finalPackage}/bin/emacsclient -e "(workspace--get-bar)" | ${pkgs.gnused}/bin/sed -e 's/^"//' -e 's/"$//'
        '';
        initial = 1;
      };
      "module/date" = {
        type = "internal/date";
        date = "%Y/%m/%d";
        time = "%H:%M";
        label = "%date% %time%";
      };
    };
  };
}
