{ pkgs, ... }: {
  xdg.configFile."common-lisp/source-registry.conf".text = ''
    (:source-registry (:tree (:home "src")) :inherit-configuration)
  '';
}
