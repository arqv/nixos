#+TITLE: NixOS configuration
This repository holds configurations for my machines running *NixOS*

** Directory structure
+ ~hosts/~: contains all host-specific files, both for *NixOS* and *home-manager*
+ ~lib/~: useful functions
+ ~modules/~: base of the configuration, all host-agnostic configuration that can be applied
  + ~modules/nixos/~: *NixOS* specific configuration
  + ~modules/home/~: *home-manager* specific configuration (a.k.a. dotfiles)
+ ~pkgs/~: custom packages used in the configuration files

** Bootstrap guide
*Do not* blindly rebuild using your system using this repository, most likely it won't work.
   
*** Requirements
- A flakes-enabled Nix installation (~nix.package = pkgs.nixFlakes~)
- Enough computing power to compile Emacs (more on how to prevent this later)

*** Steps
**** Installing from the provided ISO definition
/NOTE: this requires a working Nix installation./
1) *Build the ISO definition:*
#+BEGIN_SRC sh
  nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=iso.nix
#+END_SRC
2) *Flash the ISO to an USD/CD and boot it:* you know how to do this.
3) *Install:*
NOTE: The ~--impure~ argument is needed due to how *home-manager* generates it's documentation
(at least according to [[https://discourse.nixos.org/t/how-to-get-nixos-install-flake-to-work/10069/4][this comment]] in a NixOS forum thread)
#+BEGIN_SRC sh
  nixos-install --flake "/etc/configuration#hostname" --impure
#+END_SRC
**** Installing from a NixOS system
Clone the repository and run ~nixos-rebuild --flake .# <switch|boot>~

*** EXTRA: Preventing Emacs compilation
Since the configuration uses the ~nix-community~ binary cache for some programs (such
as Emacs), rebuilding the system from scratch will force Emacs to be compiled since it
won't be aware of the new Nix changes during the rebuild.
To fix this, comment the ~./emacs.nix~ import from the ~modules/home/base/desktop.nix~ file
and the `xsession.windowManager.command` attribute, build the system and uncomment back
to normal.
